FROM debian:bullseye-slim AS build

# COPY mysql-apt-config_0.8.14-1_all.deb /tmp/mysql-apt-config_0.8.14-1_all.deb
COPY config.expect /tmp/config.expect
RUN apt -y update && apt -y install expect lsb-release wget gnupg && cd /tmp && wget 'https://dev.mysql.com/get/mysql-apt-config_0.8.23-1_all.deb' && expect /tmp/config.expect && rm -fr /tmp/mysql-apt-config_0.8.23-1_all.deb /tmp/config.expect && apt -y update && apt -y install libmysqlclient-dev libssl-dev build-essential cpanminus

WORKDIR /app
RUN cpanm -n -l /app/perl DBD::mysql

FROM debian:latest
WORKDIR /app
ENV PERL5LIB=/app/perl
COPY --from=build /app .
COPY --from=build /etc/apt/sources.list.d/mysql.list /etc/apt/sources.list.d/mysql.list
COPY --from=build /etc/apt/trusted.gpg /etc/apt/trusted.gpg
RUN apt -y update && apt -y install lsb-release wget gnupg libmysqlclient21

